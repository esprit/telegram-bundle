<?php

namespace TelegramBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use TelegramBundle\Telegram\Bot;
use TelegramBundle\Model\StepReference;
use TelegramBundle\Model\Update;

class TelegramBotRequestEvent extends Event
{
    /** @var Bot */
    private $bot;

    /** @var StepReference */
    private $step;

    /** @var Update */
    private $update;

    public function __construct(Bot $bot, StepReference $step, Update $update)
    {
        $this->bot = $bot;
        $this->step = $step;
        $this->update = $update;
    }

    public function getBot(): Bot
    {
        return $this->bot;
    }

    public function getUpdate(): Update
    {
        return $this->update;
    }

    /**
     * @return StepReference
     */
    public function getStep(): StepReference
    {
        return $this->step;
    }
}