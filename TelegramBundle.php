<?php

namespace TelegramBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TelegramBundle\DependencyInjection\Compiler\BotCommandPass;
use TelegramBundle\DependencyInjection\Compiler\SecurityCheckerPass;
use TelegramBundle\DependencyInjection\Compiler\TelegramContextPass;

class TelegramBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new BotCommandPass());
        $container->addCompilerPass(new SecurityCheckerPass());
        $container->addCompilerPass(new TelegramContextPass());
    }
}
