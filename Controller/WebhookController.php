<?php

namespace TelegramBundle\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TelegramBundle\Telegram\BotManager;

class WebhookController
{
    private LoggerInterface $logger;
    private BotManager $manager;

    public function __construct(LoggerInterface $logger, BotManager $manager)
    {
        $this->logger = $logger;
        $this->manager = $manager;
    }

    /**
     * @Route("/webhook/{token}", name="telegram_webhook")
     */
    public function indexAction(string $token, Request $request)
    {
        $bot = $this->manager->searchBot($token);

        $this->logger->debug('Telegram webhook request: '. $request->getContent());
        $bot->handleRequest();

        return new JsonResponse([
            'ok' => true,
        ]);
    }
}
