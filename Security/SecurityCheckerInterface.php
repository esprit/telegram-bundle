<?php

namespace TelegramBundle\Security;

use TelegramBundle\Telegram\Bot;
use TelegramBundle\Model\StepReference;
use TelegramBundle\Model\Update;

interface SecurityCheckerInterface
{
    public function isGranted(StepReference $step): bool;

    public function support(Bot $bot): bool;
}