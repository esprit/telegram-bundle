<?php

namespace TelegramBundle\Security;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TelegramBundle\Event\TelegramBotRequestEvent;
use TelegramBundle\Telegram\Exception\AccessDeniedException;

class Security implements EventSubscriberInterface
{
    /** @var SecurityCheckerInterface[] */
    private $checkers;

    public function addChecker(SecurityCheckerInterface $checker)
    {
        $this->checkers[] = $checker;
    }

    public function checkSecurity(TelegramBotRequestEvent $event)
    {
        $bot = $event->getBot();
        $step = $event->getStep();

        foreach ($this->checkers as $securityChecker) {
            if ($securityChecker->support($bot) and ! $securityChecker->isGranted($step)) {
                throw new AccessDeniedException('Access denied');
            }
        }
    }

    /**
     * @uses checkSecurity
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [TelegramBotRequestEvent::class => 'checkSecurity'];
    }
}