<?php

namespace TelegramBundle\Security;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TelegramBundle\Event\TelegramBotRequestEvent;
use TelegramBundle\Telegram\Bot;
use TelegramBundle\Model\Update;
use TelegramBundle\Telegram\UserProviderInterface;

class TelegramContext implements EventSubscriberInterface
{
    /** @var UserProviderInterface */
    private $userProvider;

    /** @var Bot */
    private $bot;

    /** @var Update */
    private $update;

    private $user;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setUserProvider(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    /**
     * @uses setContext
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [TelegramBotRequestEvent::class => ['setContext', 10]];
    }

    /**
     * @param TelegramBotRequestEvent $event
     */
    public function setContext(TelegramBotRequestEvent $event)
    {
        $update = $event->getUpdate();

        $this->update = $update;
        $this->bot = $event->getBot();

        $id = $update->getChatId();
        $this->user = $this->userProvider->getTelegramUser($id);

        $this->logger->debug('Setup telegram context', [
            'bot' => $this->bot->getToken(),
            'chat_id' => $update->getChatId(),
            'user' => $this->user ? 'Resolved' : "User with telegram-id [$id] not found",
        ]);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Bot
     */
    public function getBot(): Bot
    {
        return $this->bot;
    }

    /**
     * @return Update
     */
    public function getUpdate(): Update
    {
        return $this->update;
    }
}