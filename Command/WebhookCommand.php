<?php

namespace TelegramBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;
use TelegramBundle\Telegram\Bot;
use TelegramBundle\Telegram\Api;
use TelegramBundle\Telegram\BotManager;

class WebhookCommand extends Command
{
    /** @var BotManager*/
    private $botManager;

    /** @var RouterInterface */
    private $router;

    /**
     * WebhookCommand constructor.
     * @param BotManager $botManager
     * @param RouterInterface $router
     */
    public function __construct(BotManager $botManager, RouterInterface $router)
    {
        parent::__construct();

        $this->botManager = $botManager;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('telegram:webhook')
            ->setDescription('Настраивает телеграм-бота')
            ->addOption('remove', 'r', InputOption::VALUE_NONE, 'Remove webhook')
            ->addArgument('name', InputArgument::OPTIONAL, 'Bot name')
            ->addOption('certificate', 'c', InputOption::VALUE_OPTIONAL, 'Certificate file')
            ->addOption('host', 'H', InputOption::VALUE_OPTIONAL, 'Custom host')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // https://telegram-bot-sdk.readme.io/v2.0/docs/setwebhook

        if ($name = $input->getArgument('name')) {
            $this->processBot($this->botManager->getBot($name), $input, $output);
            exit;
        }

        foreach ($this->botManager->getConfig('bots') as $name => $conf) {
            $this->processBot($this->botManager->getBot($name), $input, $output);
        }
    }

    public function processBot(Bot $bot, InputInterface $input, OutputInterface $output)
    {
        $output->writeln($bot->getToken());

        if ($input->getOption('remove')) {

            $result = $bot->getApi()->removeWebhook();
        } else {

            if (!$host = $input->getOption('host')) {
                $host = $this->botManager->getHost();
            }

            $this->router->getContext()->setHost($host);
            $this->router->getContext()->setScheme('https');

            $path = $this->router->generate('telegram_webhook', ['token' => $bot->getToken()], RouterInterface::ABSOLUTE_URL);
            $certificate = $input->getOption('certificate');

            $output->writeln($path);
            $result = $bot->getApi()->setWebhook($path, $certificate);
        }

        $output->writeln((string) $result);
    }
}
