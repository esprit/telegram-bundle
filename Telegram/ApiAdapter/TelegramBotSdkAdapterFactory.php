<?php

namespace TelegramBundle\Telegram\ApiAdapter;

class TelegramBotSdkAdapterFactory implements ApiAdapterFactoryInterface
{
    public function getAdapter(string $apiKey, $clientOptions): ApiInterface
    {
        return new TelegramBotSdkAdapter($apiKey, $clientOptions);
    }
}