<?php

namespace TelegramBundle\Telegram\ApiAdapter;

use Telegram\Bot\Api;
use Telegram\Bot\HttpClients\GuzzleHttpClient;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Message;
use TelegramBundle\Model\Update;
use GuzzleHttp\Client;

class TelegramBotSdkAdapter implements ApiInterface
{
    /** @var Api */
    private $api;

    public function __construct(string $apiKey, array $clientOptions = [])
    {
        $client = $clientOptions
            ? new GuzzleHttpClient(new Client($clientOptions))
            : null;

        $this->api = new Api($apiKey, false, $client);
    }

    public function getUpdate(): ?Update
    {
       $update = $this->api->getWebhookUpdate();

       if (! ($message = $update->getMessage())) {
           return null;
       }

       if (! $message instanceof Message) {
           return null;
       }

       return new Update($update->getChat()->get('id'), $message->text);
    }

    public function choice(string $chatId, string $text, array $choices)
    {
        $this->api->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
            'reply_markup' => Keyboard::make([
                'keyboard' => [$choices],
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
            ]),
        ]);
    }

    public function text(string $chatId, string $text)
    {
        $this->api->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
            //'reply_markup' =>  Keyboard::hide(),
        ]);

        // TODO implement it
//        if ($this->update->isType('callback_query')) {
//
//            $callback = $this->update->getCallbackQuery();
//            $message = $callback->getMessage();
//
//            $this->telegram->editMessageText([
//                'chat_id'    => $message->getChat()->getId(),
//                'message_id' => $message->getMessageId(),
//                'text'       => $this->parseText($text, $params)
//            ]);
//        }
//
//        return $this;
    }


    public function setWebhook(string $url, string $certificate = null): bool
    {
        $params = ['url' => $url];

        if ($certificate) {
            $params['certificate'] = $certificate;
        }

        return $this->api->setWebhook($params);
    }

    public function removeWebhook(): bool
    {
        return $this->api->deleteWebhook();
    }
}