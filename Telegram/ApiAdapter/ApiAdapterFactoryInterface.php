<?php

namespace TelegramBundle\Telegram\ApiAdapter;

interface ApiAdapterFactoryInterface
{
    public function getAdapter(string $apiKey, array $clientOptions): ApiInterface;
}