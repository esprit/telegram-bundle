<?php

namespace TelegramBundle\Telegram\ApiAdapter;

use TelegramBundle\Model\Update;

class DummyApiAdapter implements ApiInterface, ApiAdapterFactoryInterface
{
    public function getUpdate(): ?Update
    {
    }

    public function text(string $chatId, string $text)
    {
    }

    public function choice(string $chatId, string $text, array $choices)
    {
    }

    public function removeWebhook(): bool
    {
    }

    public function setWebhook(string $url, string $certificate = null): bool
    {
    }

    // in best world it should be two different classes for it, but I am too lazy
    public function getAdapter(string $apiKey, array $clientOptions): ApiInterface
    {
        return new DummyApiAdapter();
    }
}