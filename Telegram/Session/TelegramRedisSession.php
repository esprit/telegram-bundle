<?php

namespace TelegramBundle\Telegram\Session;

use Snc\RedisBundle\Client\Phpredis\Client;

class TelegramRedisSession implements TelegramSessionInterface
{
    /** @var \Redis */
    private $redis;

    private $prefix;

    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    public function start(string $uid)
    {
        $this->prefix = $uid;
    }

    public function set(string $key, $value)
    {
        $this->redis->hSet($this->prefix, $key, serialize($value));
    }

    public function get(string $key, $default = null)
    {
        $value = $this->redis->hGet($this->prefix, $key);

        if ($value) {
            $value = unserialize($value);
        }

        return $value ?: $default;
    }

    public function clear()
    {
        $this->redis->delete($this->prefix);
    }

    public function all()
    {
        $data = [];
        foreach ($this->redis->hKeys($this->prefix) as $key) {
            $data[$key] = $this->get($key);
        }

        return $data;
    }
}