<?php

namespace TelegramBundle\Telegram\Session;

use TelegramBundle\Telegram\Session\TelegramSessionInterface;

class TelegramSession
{
    /** @var TelegramSessionInterface */
    private $session;

    public function __construct(TelegramSessionInterface $session)
    {
        $this->session = $session;
    }

    public function init(string $token, string $id)
    {
        $id = sprintf('%s-%s', $token, $id);
        $this->session->start($id);
    }

    public function getNextStep(): array
    {
        $dialog = $this->session->get('dialog');
        $step = $this->session->get('step');

        return [$dialog, $step];
    }

    public function setNextStep(string $dialog, string $step)
    {
        $this->session->set('dialog', $dialog);
        $this->session->set('step', $step);
    }

    public function clear()
    {
       $this->session->clear();
    }

    public function set(string $name, $value)
    {
        $this->session->set($name, $value);
    }

    public function get(string $name, $default = null)
    {
        return $this->session->get($name, $default);
    }
}