<?php

namespace TelegramBundle\Telegram\Session;

interface TelegramSessionInterface
{
    public function start(string $uid);

    public function set(string $key, $value);

    public function get(string $key, $default = null);

    public function clear();

    public function all();
}