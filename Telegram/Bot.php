<?php

namespace TelegramBundle\Telegram;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TelegramBundle\Event\TelegramBotRequestEvent;
use TelegramBundle\Telegram\ApiAdapter\ApiInterface;
use TelegramBundle\Telegram\Exception\AccessDeniedException;
use TelegramBundle\Model\StepReference;
use TelegramBundle\Model\Update;
use TelegramBundle\Telegram\Command\DialogInterface;
use TelegramBundle\Telegram\Exception\CommandNotFoundException;
use TelegramBundle\Telegram\Session\TelegramSession;

class Bot
{
    /** @var TelegramSession */
    private $session;

    /** @var \Psr\Log\LoggerInterface */
    private $logger;

    /** @var string */
    private $token;

    /** @var DialogInterface[] */
    private $dialogs;

    /** @var ApiInterface */
    private $apiAdapter;

    /** @var EventDispatcherInterface */
    private $ed;

    public function __construct(string $token, ApiInterface $api, TelegramSession $session,
                                EventDispatcherInterface $ed, LoggerInterface $logger = null)
    {
        $this->session = $session;
        $this->logger = $logger;
        $this->token = $token;
        $this->apiAdapter = $api;
        $this->ed = $ed;
    }

    public function addDialog(DialogInterface $dialog)
    {
        $this->dialogs[$dialog->getName()] = $dialog;
    }

    /**
     * @param string $name
     * @return DialogInterface
     * @throws CommandNotFoundException
     */
    private function getDialogByName(string $name): DialogInterface
    {
        if (! isset($this->dialogs[$name])) {
            throw new CommandNotFoundException('Command not found');
        }

        return $this->dialogs[$name];
    }

    private function isStartCommand(string $message): ?string
    {
       if (preg_match('@/(\w+)@', $message, $matches)) {
            return $matches[1];
       }

       return null;
    }

    /**
     * @param string $message
     * @return StepReference
     * @throws CommandNotFoundException
     */
    private function getCurrentStep(string $message): StepReference
    {
        if ($startCommand = $this->isStartCommand($message)) {
            $dialog = $this->getDialogByName($startCommand);

            return new StepReference($dialog);
        }

        list($dialogName, $step) = $this->session->getNextStep();

        if (! $dialogName) {
            $dialogName = StepReference::DEFAULT_DIALOG;
        }

        return new StepReference($this->getDialogByName($dialogName), $step);
    }

    public function handleRequest()
    {
        if ($update = $this->apiAdapter->getUpdate()) {
            $this->handleUpdate($update);
        }
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function handleUpdate(Update $update)
    {
        try {
            $message = $update->getMessage();
            if (! $message) {
                return;
            }

            $chatId = $update->getChatId();

            $this->session->init($this->token, $chatId);

            $currentStep = $this->getCurrentStep($message);

            $this->ed->dispatch(new TelegramBotRequestEvent($this, $currentStep, $update));

            $nextStep = $currentStep->call($message);

            if (null !== $nextStep) {
                $this->session->setNextStep($nextStep->getDialogName(), $nextStep->getMethod());

                return;
            }
        } catch (AccessDeniedException $e) {
            $this->apiAdapter->text($update->getChatId(), 'Access denied');
        } catch (CommandNotFoundException $e) {
            $this->apiAdapter->text($update->getChatId(), 'Command not found');
        }

        $this->session->clear();
    }

    /**
     * @return DialogInterface[]
     */
    public function getCommands(): array
    {
        return $this->dialogs;
    }

    public function triggerCommand(string $command)
    {
        // TODO implement me
    }

    public function getApi(): ApiInterface
    {
        return $this->apiAdapter;
    }

    public function sendMessage(array $ids, string $message)
    {
        foreach ($ids as $id) {
            $this->apiAdapter->text($id, $message);
        }
    }
}