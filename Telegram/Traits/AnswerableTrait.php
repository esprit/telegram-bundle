<?php

namespace TelegramBundle\Telegram\Traits;

trait AnswerableTrait
{
    use ContextTrait;

    private function parseText($text, array $params = [])
    {
        if (is_array($text)) {
            $text = implode(PHP_EOL, $text);
        }

        if ($params) {
            $text = vsprintf($text, $params);
        }

        return $text;
    }

    public function text($text, ...$params)
    {
        $this->getApi()->text($this->getUpdate()->getChatId(), $this->parseText($text, $params));
    }

    public function choice($text, array $choices, ...$params)
    {
        $chatId = $this->getUpdate()->getChatId();
        $this->getApi()->choice($chatId, $this->parseText($text, $params), $choices);
    }
}