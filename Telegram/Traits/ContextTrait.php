<?php

namespace TelegramBundle\Telegram\Traits;

use TelegramBundle\Telegram\ApiAdapter\ApiInterface;
use TelegramBundle\Telegram\Bot;
use TelegramBundle\Model\Update;
use TelegramBundle\Security\TelegramContext;

trait ContextTrait
{
    /** @var TelegramContext */
    private $context;

    /**
     * @param TelegramContext $context
     */
    public function setContext(TelegramContext $context): void
    {
        $this->context = $context;
    }

    public function getUser()
    {
        return $this->context->getUser();
    }

    public function getBot(): Bot
    {
        return $this->context->getBot();
    }

    public function getUpdate(): Update
    {
        return $this->context->getUpdate();
    }

    public function getApi(): ApiInterface
    {
        return $this->getBot()->getApi();
    }
}