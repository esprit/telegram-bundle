<?php

namespace TelegramBundle\Telegram\Traits;

trait CommandTrait
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}