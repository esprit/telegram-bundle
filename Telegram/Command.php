<?php

namespace TelegramBundle\Telegram;

use TelegramBundle\Telegram\Command\DialogInterface;
use TelegramBundle\Telegram\Traits\AnswerableTrait;
use TelegramBundle\Telegram\Traits\CommandTrait;

abstract class Command implements DialogInterface
{
    use CommandTrait;
    use AnswerableTrait;
}