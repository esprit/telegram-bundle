<?php

namespace TelegramBundle\Telegram\Command;

use TelegramBundle\Security\TelegramContext;
use TelegramBundle\Telegram\Command;

class MeCommand extends Command
{
    public $name = 'me';

    public $description = 'Information about user';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $update = $this->getUpdate();
        $this->text([
            'ID: %s',
        ], $update->getChatId());
    }
}