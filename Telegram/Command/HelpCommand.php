<?php

namespace TelegramBundle\Telegram\Command;

use TelegramBundle\Security\TelegramContext;
use TelegramBundle\Telegram\Command;

class HelpCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'help';

    /**
     * @var string Command Description
     */
    protected $description = 'Справка по коммандам бота';

    /** @var TelegramContext */
    private $context;

    /**
     * HelpCommand constructor.
     * @param TelegramContext $context
     */
    public function __construct(TelegramContext $context)
    {
        $this->context = $context;
    }

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->text('Я понимаю такие команды:');

        $commands = $this->context->getBot()->getCommands();

        // Build the list
        $response = '';
        foreach ($commands as $name => $command) {
            $response .= sprintf('/%s - %s' . PHP_EOL, $name, $command->getDescription());
        }

        // Reply with the commands list
        $this->text($response);
    }
}