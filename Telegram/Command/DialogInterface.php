<?php

namespace TelegramBundle\Telegram\Command;

interface DialogInterface
{
    public function getName(): string;

    public function getDescription(): string;

    public function handle();
}