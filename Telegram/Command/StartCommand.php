<?php

namespace TelegramBundle\Telegram\Command;

use TelegramBundle\Security\TelegramContext;
use TelegramBundle\Telegram\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * @var string Command Description
     */
    protected $description = 'С этой команды начинается общение с роботом';

    /** @var TelegramContext */
    private $context;

    /**
     * StartCommand constructor.
     * @param TelegramContext $context
     */
    public function __construct(TelegramContext $context)
    {
        $this->context = $context;
    }

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->text('Привет %s! Я робот-помощник.', (string) $this->context->getUser());

        // Trigger another command dynamically from within this command
        // When you want to chain multiple commands within one or process the request further.
        // The method supports second parameter arguments which you can optionally pass, By default
        // it'll pass the same arguments that are received for this command originally.
        $this->context->getBot()->triggerCommand('help');

        return null;
    }
}