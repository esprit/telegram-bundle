<?php

namespace TelegramBundle\Telegram;

interface TelegramUserInterface
{
    public function getTelegramId();
}