<?php

namespace TelegramBundle\Telegram;

use Exception;
use Psr\Log\LoggerInterface;
use TelegramBundle\Telegram\Bot;
use TelegramBundle\Telegram\Command\DialogInterface;

class BotManager
{
    /** @var BotFactory */
    private $factory;

    /** @var array */
    private $config;

    /** @var array */
    private $bots;

    public function __construct(BotFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function searchBot($token): Bot
    {
        foreach ($this->config['bots'] as $key => $botConfig) {
            if ($botConfig['token'] == $token) {
                return $this->getBot($key);
            }
        }

        throw new \Exception('Bot not found');
    }

    public function getBot($name): Bot
    {
        if (!isset($this->bots[$name])) {
            $this->bots[$name] = $this->createBot($name);
        }

        return $this->bots[$name];
    }

    private function getBotConfig($name, $parameter = null)
    {
        if (! isset($this->config['bots'][$name])) {
            throw new Exception("Bot [$name] is not configured");
        }

        if ($parameter) {
            return $this->config['bots'][$name][$parameter];
        }

        return $this->config['bots'][$name];
    }

    public function getHost()
    {
        return $this->config['host'];
    }

    public function createBot($name): Bot
    {
        $config = $this->getBotConfig($name);

        return $this->factory->createBot($config);
    }

    /**
     * @return mixed
     */
    public function getConfig($name = null)
    {
        if ($name) {
            return $this->config[$name] ?? null;
        }

        return $this->config;
    }

    public function addCommand(string $botName, DialogInterface $dialog)
    {
        $bot = $this->getBot($botName);
        $bot->addDialog($dialog);
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}