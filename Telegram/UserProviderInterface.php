<?php

namespace TelegramBundle\Telegram;

interface UserProviderInterface
{
    public function getTelegramUser(string $id);
}