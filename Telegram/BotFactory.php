<?php

namespace TelegramBundle\Telegram;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TelegramBundle\Telegram\ApiAdapter\ApiAdapterFactoryInterface;
use TelegramBundle\Telegram\ApiAdapter\DummyApiAdapter;
use TelegramBundle\Telegram\ApiAdapter\TelegramBotSdkAdapter;
use TelegramBundle\Telegram\Bot;
use TelegramBundle\Telegram\Session\TelegramSession;
use TelegramBundle\Security\Security;

class BotFactory
{
    /** @var EventDispatcherInterface */
    private $ed;

    /** @var ApiAdapterFactoryInterface */
    private $adapterFactory;

    /** @var Security */
    private $security;

    /** @var TelegramSession */
    private $session;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(EventDispatcherInterface $ed,
                                ApiAdapterFactoryInterface $adapterFactory,
                                Security $security,
                                TelegramSession $session,
                                LoggerInterface $logger = null)
    {
        $this->adapterFactory = $adapterFactory;
        $this->security = $security;
        $this->session = $session;
        $this->ed = $ed;
        $this->logger = $logger;
    }

    public function createBot(array $config)
    {
        $token = $config['token'];
        $apiKey = $config['apiKey'];
        $client_options = $config['http_client'];

        $apiAdapter = $this->adapterFactory->getAdapter($apiKey, $client_options);

        return new Bot($token, $apiAdapter, $this->session, $this->ed, $this->logger);
    }
}