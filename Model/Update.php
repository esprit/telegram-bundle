<?php

namespace TelegramBundle\Model;

class Update
{
    /** @var string */
    private $chatId;

    /** @var string */
    private $message;

    /**
     * Update constructor.
     * @param string $chatId
     * @param string $message
     */
    public function __construct(string $chatId, string $message = null)
    {
        $this->chatId = $chatId;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getChatId(): string
    {
        return $this->chatId;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}