<?php

namespace TelegramBundle\Model;

use TelegramBundle\Telegram\Command\DialogInterface;

class StepReference
{
    /** @var DialogInterface */
    private $dialog;

    /** @var string */
    private $method;

    const DEFAULT_METHOD = 'handle';
    const DEFAULT_DIALOG = 'help';

    public function __construct(DialogInterface $dialog, string $method = null)
    {
        $this->dialog = $dialog;
        $this->method = $method ?: self::DEFAULT_METHOD;
    }

    public function getDialogName(): string
    {
        return $this->dialog->getName();
    }

    public function call($message): ?StepReference
    {
        $nextStep =  call_user_func([$this->dialog, $this->method], $message);

        if ($nextStep === null) {
            return null;
        }

        return new StepReference($this->dialog,  $nextStep);
    }

    public function getMethod(): string
    {
        return $this->method;
    }
}