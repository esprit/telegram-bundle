<?php

namespace TelegramBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('telegram');
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('host')->end()
                ->scalarNode('user_provider')->end()
                ->arrayNode('bots')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('apiKey')->end()
                            ->scalarNode('token')->end()
                            ->arrayNode('roles')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('http_client')
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
