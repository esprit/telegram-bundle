<?php

namespace TelegramBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SecurityCheckerPass implements CompilerPassInterface
{
    /**
     * @uses \TelegramBundle\Security\Security::addChecker()
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('telegram.security')) {
            return;
        }

        $definition = $container->findDefinition('telegram.security');
        $taggedServices = $container->findTaggedServiceIds('telegram_security');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addChecker', [
                new Reference($id)
            ]);
        }
    }
}