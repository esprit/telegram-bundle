<?php

namespace TelegramBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class TelegramContextPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $config = $container->getExtensionConfig('telegram');

        $userStorage = $container->findDefinition('telegram.context');
        $userStorage->addMethodCall('setUserProvider', [new Reference($config[0]['user_provider'])]);
    }
}