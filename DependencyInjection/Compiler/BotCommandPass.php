<?php

namespace TelegramBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BotCommandPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has('telegram')) {
            return;
        }

        $telegram = $container->findDefinition('telegram');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('telegram_command');

        foreach ($taggedServices as $id => $tags) {

            $command = $container->findDefinition($id);
            $command->addMethodCall('setContext', [new Reference('telegram.context')]);

            // a service could have the same tag twice
            foreach ($tags as $attributes) {
                $telegram->addMethodCall('addCommand', [
                    $attributes["bot"],
                    new Reference($id)
                ]);
            }
        }
    }
}