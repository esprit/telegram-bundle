<?php


namespace TelegramBundle\Sender;


class TelegramMessage
{
    /** @var string */
    private $bot;

    /** @var array */
    private $recipients;

    /** @var string */
    private $message;

    public function __construct(string $bot, array $recipients, string $message)
    {
        $this->bot = $bot;
        $this->recipients = $recipients;
        $this->message = $message;
    }

    public function getBot(): string
    {
        return $this->bot;
    }

    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}