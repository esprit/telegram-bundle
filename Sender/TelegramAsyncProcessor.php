<?php

namespace TelegramBundle\Sender;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use TelegramBundle\Telegram\BotManager;

class TelegramAsyncProcessor implements ConsumerInterface
{
    const TOPIC_NAME = 'telegram';

    /** @var BotManager */
    private $botManager;

    /**
     * TelegramAsyncProcessor constructor.
     * @param BotManager $botManager
     */
    public function __construct(BotManager $botManager)
    {
        $this->botManager = $botManager;
    }

    private function sendMessage(TelegramMessage $message)
    {
        $bot = $this->botManager->getBot($message->getBot());
        $ids = $message->getRecipients();

        $bot->sendMessage($ids, $message->getMessage());
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $message)
    {
        $this->sendMessage(unserialize($message->getBody()));
    }
}